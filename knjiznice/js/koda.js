
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
$(document).ready(function(){
  
    $('.polje').change(function(){
      if ($('#imeBlonika').val().length!=0 && $('#priimekBlonika').val().length!=0 && $('#rojstvoBlonika').val().length!=0 && $('#spolBlonika').val().length!=0) {
      document.getElementById("genEHR").disabled=false;
      } else {
      document.getElementById("genEHR").disabled=true;
      }
    });
  
  $('#obstojeciBolnik').change(function() {
    var sekanjePodatkov = $(this).val().split(';');
    $('#imeBlonika').val(sekanjePodatkov[0]);
    $('#priimekBlonika').val(sekanjePodatkov[1]);
    $('#rojstvoBlonika').val(sekanjePodatkov[2]);
    
    if ($('#imeBlonika').val().length!=0 && $('#priimekBlonika').val().length!=0 && $('#rojstvoBlonika').val().length!=0 && $('#spolBlonika').val().length!=0) {
      document.getElementById("genEHR").disabled=false;
    } else {
      document.getElementById("genEHR").disabled=true;
    }
  });
  
  $('#generiranPacient').change(function() {
    //console.log("zazna");
    var ehr = $(this).val();
      $("#pregledEHRId").val(ehr);
  });
  
});



function genEHR() {
  var ime = $('#imeBlonika').val();
  var priimek = $('#priimekBlonika').val();
  var rojstvo = $('#rojstvoBlonika').val();
  var spol = $('#spolBlonika').val();
  
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      var ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: rojstvo,
        gender: spol,
        partyAdditionalInfo: [
          {
            key: "ehrId",
            value: ehrId
          }
        ]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function (party) {
          if (party.action == 'CREATE') {
            $("#povratnoSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
          }
        },
        error: function(err) {
          $('#povratnoSporocilo').html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
        }
      });
    }
  });
}

function noveMeritve() {
  var ehrId = $('#ehrId').val();
  var datum = $('#datumMeritev').val();
  var visina = $('#visinaMeritev').val();
  var teza = $('#tezaMeritev').val();
  var temp = $('#tempMeritev').val();
  var puls = $('#utripMeritev').val();
  
  var vpisMeritev = {
    "ctx/time": datum,
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": teza,
    "vital_signs/body_temperature/any_event/temperature|magnitude": temp,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/pulse/any_event/rate": puls
  };
  var params = {
    ehrId: ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'dr. Phil'
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(params),
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify(vpisMeritev),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {
      $('#povratnoSporocilo2').html("<span class='obvestilo label label-success fade-in'>Uspešno vneseni podatki! Pregled meritev možen z vašim EHRid: " + ehrId + "</span>");
      
        $('#ehrId').val("");
        $('#datumMeritev').val("");
        $('#visinaMeritev').val("");
        $('#tezaMeritev').val("");
        $('#tempMeritev').val("");
        $('#utripMeritev').val("");
    }
  });
}

function preglejMeritve() {
  on();
  izpisMeritev();
}
// Izpis meritev
function izpisMeritev() {
var ehrId = $('#pregledEHRId').val();
console.log(ehrId);
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

//izpis puls
  $.ajax({
      url: baseUrl + "/view/" + ehrId + "/pulse",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
  
          res.forEach(function (el, i, arr) {
              var date = new Date(el.time);
              el.date = date.getTime();
          });
  
          new Morris.Area({
              element: 'chart-pulse',
              data: res.reverse(),
              xkey: 'date',
              ykeys: ['pulse'],
              lineColors: ['#A0D468'],
              labels: ['Pulse'],
              lineWidth: 2,
              pointSize: 3,
              hideHover: true,
              behaveLikeLine: true,
              smooth: false,
              resize: true,
              xLabels: "day",
              xLabelFormat: function (x){
                  var date = new Date(x);
                  return (date.getDate() + '-' + monthNames[date.getMonth()]);
              },
              // dateFormat: function (x){
              //     return (formatDate(x, false));
              // }
          });
      }
  });
  
//izpis visina
  /*$.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {
        res.forEach(function(el, i, arr) {
            var date = new Date(el.time);
            el.date = date.getDate() + '-' + monthNames[date.getMonth()];
        });

        new Morris.Bar({
            element: 'chart-height',
            data: res.reverse(),
            xkey: 'date',
            ykeys: ['height'],
            labels: ['Height'],
            hideHover: true,
            barColors: ['#48CFAD', '#37BC9B'],
            xLabelMargin: 5,
            resize: true
        });
    }
  });*/
  
//izpis teza
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {

        res.forEach(function (el, i, arr) {
            var date = new Date(el.time);
            el.date = date.getTime();
        });

        new Morris.Area({
            element: 'chart-weight',
            data: res.reverse(),
            xkey: 'date',
            ykeys: ['weight'],
            lineColors: ['#4FC1E9'],
            labels: ['Weight'],
            lineWidth: 2,
            pointSize: 3,
            hideHover: true,
            behaveLikeLine: true,
            smooth: false,
            resize: true,
            xLabels: "day",
            xLabelFormat: function (x){
                var date = new Date(x);
                return (date.getDate() + '-' + monthNames[date.getMonth()]);
            },
            // dateFormat: function (x){
            //     return (formatDate(x, false));
            // }
        });

    }
  });
  
//izpis temeratura
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/body_temperature",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {

        res.forEach(function(el, i, arr) {
            var date = new Date(el.time);
            el.date = date.getDate() + '-' + monthNames[date.getMonth()];
        });

        new Morris.Bar({
            element: 'body-temperature',
            data: res.reverse(),
            xkey: 'date',
            ykeys: ['temperature'],
            labels: ['Body Temperature'],
            hideHover: true,
            barColors: ['#FFCE54'],
            xLabelMargin: 5,
            resize: true
        });

      }
    });
  
// izpis IDE
var telTeza = [];
var izracun = [];
var prikazTabele = "<table class='tabela'><tr><th>Datum in ura meritve</th><th class='text-right'>Index telesne mase</th></tr>";
$.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {
        for (var i in res) {
          //telTeza[i] = res[i].time;
          telTeza[i] = res[i].weight;

        }
        $.ajax({
          url: baseUrl + "/view/" + ehrId + "/height",
          type: 'GET',
          headers: {
            "Authorization": getAuthorization()
          },
          success: function (res) {
            for (var i in res) {
              izracun = telTeza[i]/(res[i].height*res[i].height) * 10000;
              var zaokrozen = Math.round(izracun*10)/10;
              //$("#itm").append(res[i].time + ': ' + zaokrozen + "<br>");
              prikazTabele += "<tr><td>" + res[i].time + "</td><td class='text-right'>" + zaokrozen + "</td></tr>";
            }
            prikazTabele+="</table>";
            console.log(prikazTabele);
            $("#itm").append(prikazTabele);
          }
        });
    }
});
    

}



function genPodatke() {
  setTimeout(function() {generirajPodatke(1);}, 0);
  setTimeout(function() {generirajPodatke(2);}, 1200);
  setTimeout(function() {generirajPodatke(3);}, 2400);
}

 
function generirajPodatke(stPacienta) {
  //ehrId = "";
  if(stPacienta==1) {
      genEHRpacient("Lojze", "Vinček", "1962-12-24", "MALE");
  }
  else if(stPacienta==2) {  
      genEHRpacient("Jure", "Grm", "1987-02-01", "MALE");
  }
  else if(stPacienta==3) {
      genEHRpacient("Damjan", "Leska", "1980-04-04", "MALE");
  }

  // TODO: Potrebno implementirati

  //return ehrId;
}

function genEHRpacient(ime, priimek, rojstvo, spol) {
  //console.log(ime);
  
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      var ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: rojstvo,
        gender: spol,
        partyAdditionalInfo: [
          {
            key: "ehrId",
            value: ehrId
          }
        ]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function (party) {
          if (party.action == 'CREATE') {
            $("#generiranPacient").append("<option value='"+ ehrId +"'>" + ime + " " + priimek + "</option>");
            console.log("sprejeto");
            setTimeout(function() {meritveRandom(ehrId)}, 200);
            setTimeout(function() {meritveRandom(ehrId)}, 400);
            setTimeout(function() {meritveRandom(ehrId)}, 600);
            setTimeout(function() {meritveRandom(ehrId)}, 800);
            setTimeout(function() {meritveRandom(ehrId)}, 1000);
          }
        },
        error: function(err) {
          $('#povratnoSporocilo').html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
        }
      });
    }
  });
  
// random meritve

  
}

function meritveRandom(preneseniEHR) {
  console.log(preneseniEHR);
  //for(var i=0; i<5; i++) {
    console.log("for");
    var dan = (Math.floor(Math.random() * 29) + 1);
    var fixDan = (dan < 10 ? '0' : '') + dan;
    var mesc = (Math.floor(Math.random() * 13) + 1);
    var fixMesc = (mesc < 10 ? '0' : '') + mesc;
    var ura = (Math.floor(Math.random() * 25));
    var fixUra = (ura < 10 ? '0' : '') + ura;
    var minuta = (Math.floor(Math.random() * 61));
    var fixMinuta = (minuta < 10 ? '0' : '') + minuta;
    
    var datum = (Math.floor(Math.random() * 30) + 1990) + "-" + fixMesc + "-" + fixDan + "T" + fixUra + ":" + fixMinuta + "Z";
    var visina = Math.floor(Math.random() * 60) + 150;
    var teza = parseFloat(((Math.random() * 70) + 50).toFixed(2));
    var temp = parseFloat(((Math.random() * 12) + 33).toFixed(2));
    var puls = Math.floor(Math.random() * 165) + 45;
  
  console.log(datum);
  console.log(visina);
  console.log(teza);
  console.log(temp);
  console.log(puls);
    
    var vpisMeritev = {
      "ctx/time": datum,
      "ctx/language": "en",
      "ctx/territory": "SI",
      "vital_signs/height_length/any_event/body_height_length": visina,
      "vital_signs/body_weight/any_event/body_weight": teza,
      "vital_signs/body_temperature/any_event/temperature|magnitude": temp,
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      "vital_signs/pulse/any_event/rate": puls
    };
    var params = {
      ehrId: preneseniEHR,
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: 'dr. Phil'
    };
    console.log(params);
    $.ajax({
      url: baseUrl + "/composition?" + $.param(params),
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vpisMeritev),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(res) {
        console.log("dela");
      }
    });
  //}
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija



// MAPA
/* global L, distance */
var mapa;
var layer;
var obmocje;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

window.addEventListener('load', function () {
  

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
  
  //var popup = L.popup();

  
  zahtevajBolnisnice(function(response) {
    var poligoni = JSON.parse(response);
    
      var poligon = L.geoJSON(poligoni, {
        
        onEachFeature: onEachFeature
      }).addTo(mapa);
  
  console.log(poligon);
      
  });
  
  // Klik na mapo
  var popup = L.popup();
  function lokacija(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Vaša lokacija")
      .openOn(mapa);
  }
  mapa.on("click", lokacija);
  
  
    // Oddaljenost
  function oddaljenost() {
    obmocje = L.circle(lokacija(), {
      radius: 3000
    });
  }
  

});




// Popup funkcija
function onEachFeature(feature, layer) {
  if((feature.properties.name ||
    feature.properties["addr:street"] ||
    feature.properties["addr:housenumber"] ||
    feature.properties["addr:postcode"] ||
    feature.properties["addr:city"]) != null) 
    {
      
    layer.bindPopup(
      feature.properties.name+ "</br>" +
      feature.properties["addr:street"]+ " " +
      feature.properties["addr:housenumber"]+ ", " +
      feature.properties["addr:postcode"]+ " " +
      feature.properties["addr:city"]);
  } else {
    layer.bindPopup("Neznani podatki");
  }
}


// Zahteva JSON
function zahtevajBolnisnice(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "knjiznice/json/bolnisnice.json", true);
  
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}




// overlay
function on() {
  document.getElementById("overlay").style.display = "block";
}

function off() {
  document.getElementById("overlay").style.display = "none";
  document.getElementById("chart-pulse").innerHTML="";
  //document.getElementById("chart-height").innerHTML="";
  document.getElementById("chart-weight").innerHTML="";
  document.getElementById("body-temperature").innerHTML="";
  document.getElementById("itm").innerHTML="";
}

// izbris X
function scroll() {
  console.log("scrolla");
  if($(this).scrollTop()>10) {
    $("#X").css({"display": "none"});
  }
}